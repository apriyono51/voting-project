import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateRolesDto } from './dto/create-roles.dto';
import { UpdateRolesDto } from './dto/update-roles.dto';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Roles } from './entities/roles.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Roles)
    private rolesRepository: Repository<Roles>,
  ) {}

  async create(createRolesDto: CreateRolesDto) {
    try {
      const isExist = await this.rolesRepository.findOne({
        where: {
          name: createRolesDto.name,
        },
      });

      if (isExist) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Failed to create role',
          },
          HttpStatus.NOT_FOUND,
        );
      }

      const result = await this.rolesRepository.insert(createRolesDto);

      return this.rolesRepository.findOneOrFail({
        where: {
          id: result.identifiers[0].id,
        },
      });
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Failed to create role',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw error;
      }
    }
  }

  async findAll() {
    try {
      const data = await this.rolesRepository
        .createQueryBuilder('roles')
        .where("roles.name NOT IN('superadmin', 'guest')")
        .getMany();

      return data;
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw error;
      }
    }
  }

  async findOne(id: string) {
    try {
      const data = await this.rolesRepository.findOne({ where: { id } });

      return data;
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: 'Data not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async update(id: string, updateRolesDto: UpdateRolesDto) {
    try {
      await this.rolesRepository.findOneOrFail({
        where: {
          id,
        },
      });

      await this.rolesRepository.update(id, updateRolesDto);

      return await this.rolesRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async delete(id: string) {
    try {
      await this.rolesRepository.delete(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }
}
