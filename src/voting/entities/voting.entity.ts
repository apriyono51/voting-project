import {
  BaseEntity,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Users } from '../../users/entities/users.entity';
import { Kandidat } from '../../kandidat/entities/kandidat.entity';

@Entity()
export class Voting extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(
    () => {
      return Users;
    },
    (user) => {
      return user.id;
    },
  )
  userId: string;

  @ManyToOne(
    () => {
      return Kandidat;
    },
    (kandidat) => {
      return kandidat.id;
    },
  )
  kandidatId: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @VersionColumn()
  version: number;
}
