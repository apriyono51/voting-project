import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUsersDto } from './dto/create-users.dto';
import { UpdateUsersDto } from './dto/update-users.dto';
import { Brackets, EntityNotFoundError, ILike, Repository } from 'typeorm';
import { Users } from './entities/users.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as uuid from 'uuid';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { hashPassword } from '../helper/hash_password';
import { Roles } from 'src/roles/entities/roles.entity';
import { UpdateUserRolesDto } from './dto/update-user-roles.dto';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
    @InjectRepository(Roles)
    private roleRepository: Repository<Roles>,
  ) {}

  async create(createUsersDto: CreateUsersDto) {
    try {
      await this.validate(createUsersDto);

      let roles;

      if (createUsersDto.roleId) {
        roles = await this.roleRepository.findOneBy({
          id: createUsersDto.roleId,
        });
      } else {
        roles = await this.roleRepository.findOne({
          where: {
            name: ILike('%user%'),
          },
        });
      }

      const salt = uuid.v4();

      return this.usersRepository.insert({
        ...createUsersDto,
        password: await hashPassword(createUsersDto.password, salt),
        salt: salt,
        roles: roles
      });
    } catch (error) {
      throw error;
    }
  }

  async findByUsername(username) {
    return this.usersRepository.findOne({ where: { username } });
  }

  findByEmail(email) {
    return this.usersRepository.findOne({
      where: {
        email,
      },
      relations: ['roles'],
    });
  }

  findByUsernameOrEmail(usernameOrEmail) {
    return this.usersRepository
      .createQueryBuilder('users')
      .leftJoinAndSelect('users.roles', 'roles', 'roles.id = users.roles_id')
      .andWhere(
        new Brackets((query) => {
          query
            .where('username = :username', {
              username: usernameOrEmail,
            })
            .orWhere('email = :email', {
              email: usernameOrEmail,
            });
        }),
      )
      .getOne();
  }

  async findAll() {
    return await this.usersRepository
      .createQueryBuilder('users')
      .select(['id', 'username', 'name', 'email', 'region_code'])
      .where("username NOT IN('superadmin', 'guest', 'sipdadmin')")
      .orderBy('region_code', 'ASC')
      .execute();
  }

  async findOne(id: string) {
    try {
      const data = await this.usersRepository.findOneOrFail({
        where: { id },
        relations: ['roles'],
        select: ['id', 'username', 'name', 'email', 'classId', 'roles'],
      });

      return data;
    } catch (e) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: 'User not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async profile(id) {
    const user = await this.findOne(id);

    delete user['password'];
    delete user['salt'];

    return user;
  }

  async updateUsers(id: string, updateUserDto: UpdateUsersDto) {
    try {
      await this.usersRepository.findOneOrFail({
      where:{
        id
      }
    });
      await this.validate(updateUserDto);

      await this.usersRepository.update(id, updateUserDto);

      const data = await this.usersRepository.findOneOrFail({ where: { id } });

      delete data['password'];
      delete data['salt'];
      delete data['forgotPasswordCode'];

      return data;
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'User not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async updateUserRoles(id: string, updateUserRolesDto: UpdateUserRolesDto) {
    try {
      const user = await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });

      const role = await this.roleRepository.findOneOrFail({
        where: {
          id: updateUserRolesDto.roleId,
        },
      });

      user.roles = role;

      await this.usersRepository.save(user);
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw error;
      }
    }
  }

  async updatePassword(
    id: string,
    updateUserPasswordDto: UpdateUserPasswordDto,
  ) {
    try {
      const user = await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });

      user.password = await hashPassword(
        updateUserPasswordDto.password,
        user.salt,
      );
      await this.usersRepository.save(user);

      const data = await this.usersRepository.findOneOrFail({ where: { id } });

      delete data['password'];
      delete data['salt'];
      delete data['forgotPasswordCode'];

      return data;
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'User not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw error;
      }
    }
  }

  async remove(id: string) {
    try {
      // const data = await this.usersRepository.findOneOrFail({ where: { id } });

      await this.usersRepository.delete(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'User not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async validate(userDto: CreateUsersDto | UpdateUsersDto) {
    try {
      if (userDto.email) {
        const isExistEmail = await this.findByEmail(userDto.email);

        if (isExistEmail) {
          throw new HttpException(
            {
              statusCode: HttpStatus.BAD_REQUEST,
              message: 'email is exist',
            },
            HttpStatus.BAD_REQUEST,
          );
        }
      }

      if (userDto.username) {
        const isExistUsername = await this.findByUsername(userDto.username);

        if (isExistUsername) {
          throw new HttpException(
            {
              statusCode: HttpStatus.BAD_REQUEST,
              message: 'username is exist',
            },
            HttpStatus.BAD_REQUEST,
          );
        }
      }
    } catch (error) {
      throw error;
    }
  }
}
