import { Module } from '@nestjs/common';
import { KandidatService } from './kandidat.service';
import { KandidatController } from './kandidat.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Kandidat } from './entities/kandidat.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Kandidat])],
  controllers: [KandidatController],
  providers: [KandidatService],
  exports: [KandidatService],
})
export class KandidatModule {}
