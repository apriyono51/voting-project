import {Body, Controller, Get, HttpStatus, Post, UseGuards} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dto/login-user.dto';
import { UsersService } from '../users/users.service';
import { CreateUsersDto } from '../users/dto/create-users.dto';
import { Public } from './guard/public.decorator';
import { ApiTags } from "@nestjs/swagger";
import {JwtAuthGuard} from "./guard/jwt-auth.guard";

@ApiTags('Auth')
@Controller('/auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Public()
  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto) {
    try {
      return {
        status: HttpStatus.OK,
        message: 'Login Successful',
        data: await this.authService.login(loginUserDto),
      };
    } catch (error) {
      return error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('register')
  async register(@Body() createUserDto: CreateUsersDto) {
    try {
        await this.usersService.create(createUserDto);

        return {
          status: HttpStatus.CREATED,
          message: 'Success create SIPD Account',
        };
    } catch (error) {
        return error
    }
  }
}
