import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UpdateClassDto } from './dto/update-class.dto';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Class } from './entities/class.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateClassDto } from './dto/create-class.dto';

@Injectable()
export class ClassService {
  constructor(
    @InjectRepository(Class)
    private classRepository: Repository<Class>,
  ) {}

  async create(createClassDto: CreateClassDto) {
    try {
      const isExist = await this.classRepository.findOne({
        where: {
          name: createClassDto.name,
        },
      });

      if (isExist) {
        throw new HttpException(
          {
            statusCode: HttpStatus.BAD_REQUEST,
            message: 'Classalready exist',
          },
          HttpStatus.BAD_REQUEST,
        );
      }

      const result = await this.classRepository.insert(createClassDto);

      return this.classRepository.findOneOrFail({
        where: {
          id: result.identifiers[0].id,
        },
      });
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Failed to create role',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw error;
      }
    }
  }

  async findAll() {
    try {
      const data = await this.classRepository
        .createQueryBuilder('roles')
        .getMany();

      return data;
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw error;
      }
    }
  }

  async findOne(id: string) {
    try {
      const data = await this.classRepository.findOne({ where: { id } });

      return data;
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_FOUND,
          message: 'Data not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  async update(id: string, updateClassDto: UpdateClassDto) {
    try {
      await this.classRepository.findOneOrFail({
        where: {
          id,
        },
      });

      await this.classRepository.update(id, updateClassDto);

      return await this.classRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async delete(id: string) {
    try {
      await this.classRepository.delete(id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }
}
