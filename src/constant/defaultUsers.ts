export const defaultUsers = [
  {
    username: 'admin',
    name: 'admin',
    email: 'admin@gmail.com',
    password: 'admin123',
    roleId: 'ac14198d-bd0c-43a3-a81d-c923fae33b22',
    classId: '',
  },
  {
    username: 'user01',
    name: 'user',
    email: 'user@gmail.com',
    password: 'user123',
    roleId: '682a659e-3c78-420a-bce7-ad971e412589',
    classId: '29557b6e-4afd-48d7-ac77-90b598f8c3ec',
  },
];
