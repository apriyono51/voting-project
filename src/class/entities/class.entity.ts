import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  VersionColumn,
  CreateDateColumn,
  BaseEntity,
} from 'typeorm';

@Entity()
export class Class extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
    id: string;

  @Column({
    unique: true,
  })
    name: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
    createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
    updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
    deletedAt: Date;

  @VersionColumn()
    version: number;
}
