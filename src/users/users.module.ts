import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { Users } from './entities/users.entity';
import { RolesModule } from '../roles/roles.module';
import { AuthModule } from '../auth/auth.module';
import { Roles } from 'src/roles/entities/roles.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Users, Roles]),
    forwardRef(() => {
      return RolesModule;
    }),
    forwardRef(() => {
      return AuthModule;
    }),
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
