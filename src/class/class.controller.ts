import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpStatus,
  ParseUUIDPipe,
  Put,
} from '@nestjs/common';
import { ClassService } from './class.service';
import { CreateClassDto } from './dto/create-class.dto';
import { UpdateClassDto } from './dto/update-class.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Class')
@Controller('class')
export class ClassController {
  constructor(private readonly classService: ClassService) {}

  @Post()
  async create(@Body() createroleDto: CreateClassDto) {
    try {
      return {
        data: await this.classService.create(createroleDto),
        status: HttpStatus.CREATED,
        message: 'Role created',
      };
    } catch (error) {
      return error;
    }
  }

  @Get()
  async findAll() {
    try {
      const data = await this.classService.findAll();

      return {
        data,
        status: HttpStatus.OK,
        message: 'success',
      };
    } catch (error) {
      return error;
    }
  }

  @Get('/:id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    try {
      return {
        data: await this.classService.findOne(id),
        status: HttpStatus.OK,
        message: 'success',
      };
    } catch (error) {
      return error;
    }
  }

  @Put('/:id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateClassDto: UpdateClassDto,
  ) {
    try {
      return {
        data: await this.classService.update(id, updateClassDto),
        status: HttpStatus.OK,
        message: 'Role updated',
      };
    } catch (error) {
      return error;
    }
  }

  @Delete('/:id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    try {
      await this.classService.delete(id);

      return {
        status: HttpStatus.OK,
        message: 'Role deleted',
      };
    } catch (error) {
      return error;
    }
  }
}
