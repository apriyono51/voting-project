import { Injectable } from '@nestjs/common';
import { CreateKandidatDto } from './dto/create-kandidat.dto';
import { UpdateKandidatDto } from './dto/update-kandidat.dto';

@Injectable()
export class KandidatService {
  create(createKandidatDto: CreateKandidatDto) {
    return 'This action adds a new kandidat';
  }

  findAll() {
    return `This action returns all kandidat`;
  }

  findOne(id: number) {
    return `This action returns a #${id} kandidat`;
  }

  update(id: number, updateKandidatDto: UpdateKandidatDto) {
    return `This action updates a #${id} kandidat`;
  }

  remove(id: number) {
    return `This action removes a #${id} kandidat`;
  }
}
