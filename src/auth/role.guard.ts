import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Role } from 'src/constant/role.enum';
import { ROLES_KEY } from 'src/utils/role-validator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector, private jwtService: JwtService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
      const requiredRoles = this.reflector.getAllAndOverride<Role[]>(
        ROLES_KEY,
        [context.getHandler(), context.getClass()],
      );

      if (!requiredRoles) {
        return true;
      }

      type PayloadType = {
        sub: string;
        roles;
      };

      const req = context.switchToHttp().getRequest();

      const data = this.jwtService.decode(
        req.headers.authorization.split(' ')[1],
      ) as PayloadType;

      return requiredRoles.some((role) => {
        return data.roles.includes(role);
      });
    } catch (error) {
      throw new HttpException(
        {
          statusCode: HttpStatus.FORBIDDEN,
          error: 'Cannot access this source',
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
