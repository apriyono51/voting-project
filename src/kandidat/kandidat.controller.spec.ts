import { Test, TestingModule } from '@nestjs/testing';
import { KandidatController } from './kandidat.controller';
import { KandidatService } from './kandidat.service';

describe('KandidatController', () => {
  let controller: KandidatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KandidatController],
      providers: [KandidatService],
    }).compile();

    controller = module.get<KandidatController>(KandidatController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
