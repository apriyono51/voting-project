import { PartialType } from '@nestjs/swagger';
import { CreateKandidatDto } from './create-kandidat.dto';

export class UpdateKandidatDto extends PartialType(CreateKandidatDto) {}
