import { SetMetadata } from "@nestjs/common";
import { Role } from "src/constant/role.enum";

export const ROLES_KEY = 'roles'
export const hasRoles = (...role: Role[]) => SetMetadata(ROLES_KEY, role)