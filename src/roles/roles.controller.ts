import {
  Controller,
  Body,
  HttpStatus,
  Post,
  Get,
  Put,
  Param,
  ParseUUIDPipe,
  Delete,
} from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRolesDto } from './dto/create-roles.dto';
import { UpdateRolesDto } from './dto/update-roles.dto';
import { hasRoles } from 'src/utils/role-validator';
import { Role } from 'src/constant/role.enum';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Roles')
@hasRoles(Role.admin)
@Controller('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @Post()
  async create(@Body() createRolesDto: CreateRolesDto) {
    try {
      return {
        data: await this.rolesService.create(createRolesDto),
        status: HttpStatus.CREATED,
        message: 'Role created',
      };
    } catch (error) {
      return error;
    }
  }

  @Get()
  async findAll() {
    try {
      const data = await this.rolesService.findAll();

      return {
        data,
        status: HttpStatus.OK,
        message: 'success',
      };
    } catch (error) {
      return error;
    }
  }

  @Get('/:id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    try {
      return {
        data: await this.rolesService.findOne(id),
        status: HttpStatus.OK,
        message: 'success',
      };
    } catch (error) {
      return error;
    }
  }

  @Put('/:id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateRolesDto: UpdateRolesDto,
  ) {
    try {
      return {
        data: await this.rolesService.update(id, updateRolesDto),
        status: HttpStatus.OK,
        message: 'Role updated',
      };
    } catch (error) {
      return error;
    }
  }

  @Delete('/:id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    try {
      await this.rolesService.delete(id);

      return {
        status: HttpStatus.OK,
        message: 'Role deleted',
      };
    } catch (error) {
      return error;
    }
  }
}
