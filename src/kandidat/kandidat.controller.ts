import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { KandidatService } from './kandidat.service';
import { CreateKandidatDto } from './dto/create-kandidat.dto';
import { UpdateKandidatDto } from './dto/update-kandidat.dto';

@Controller('kandidat')
export class KandidatController {
  constructor(private readonly kandidatService: KandidatService) {}

  @Post()
  create(@Body() createKandidatDto: CreateKandidatDto) {
    return this.kandidatService.create(createKandidatDto);
  }

  @Get()
  findAll() {
    return this.kandidatService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.kandidatService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateKandidatDto: UpdateKandidatDto) {
    return this.kandidatService.update(+id, updateKandidatDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.kandidatService.remove(+id);
  }
}
