import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
} from '@nestjs/common';
import { LoginUserDto } from './dto/login-user.dto';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { hashPassword } from '../helper/hash_password';
import { NotFoundError } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(
    @Inject(
      forwardRef(() => {
        return UsersService;
      }),
    )
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(loginUserDto: LoginUserDto) {
    try {
      
      const user = await this.usersService.findByEmail(
        loginUserDto.email,
      );


      if (!user) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Your account is not active',
          },
          HttpStatus.NOT_FOUND,
        );
      }

      if (
        user.password !== (await hashPassword(loginUserDto.password, user.salt))
      ) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Incorrect Password',
          },
          HttpStatus.NOT_FOUND,
        );
      }

      const payload = {
        email: user.email,
        username: user.username,
        sub: user.id,
        roles: user.roles.name,
        className: user.classId
      };

      return {
        username: user.username,
        access_token: this.jwtService.sign(payload, {secret: process.env.SECRET}),
      };
    } catch (error) {
       if(error instanceof NotFoundError){
        throw new HttpException({
          statusCode: HttpStatus.NOT_FOUND,
          message: "User not found"
        }, HttpStatus.NOT_FOUND
        )
      }else{
        throw error
      }
    }
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findByEmail(email);

    if (user && user.password === (await hashPassword(password, user.salt))) {
      const { password, ...result } = user;

      return result;
    }

    return null;
  }
}
