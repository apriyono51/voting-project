import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateRolesDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
}
