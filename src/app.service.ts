import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { defaultRoles } from './constant/defaultRoles';
import { defaultUsers } from './constant/defaultUsers';
import { defaultClass } from './constant/defaultClass';
import { RolesService } from './roles/roles.service';
import { UsersService } from './users/users.service';
import { ClassService } from './class/class.service';

@Injectable()
export class AppService implements OnApplicationBootstrap {
  constructor(
    private rolesService: RolesService,
    private usersService: UsersService,
    private classService: ClassService,
  ) {}

  async onApplicationBootstrap() {
    await this.seed();
  }

  async seed() {
    defaultClass.map(async (role) => {
      const data = await this.classService.findOne(role.id);

      if (!data) {
        await this.classService.create(role);
      }
    });

    defaultRoles.map(async (role) => {
      const data = await this.rolesService.findOne(role.id);

      if (!data) {
        await this.rolesService.create(role);
      }
    });

    defaultUsers.map(async (user) => {
      const data = await Promise.all([
        this.usersService.findByUsernameOrEmail(user.email),
        this.usersService.findByUsernameOrEmail(user.username),
      ]);

      if (!data[0]) {
        await this.usersService.create(user);
      }
    });
  }
}
