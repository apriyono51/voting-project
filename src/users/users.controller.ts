import {
  Controller,
  Body,
  HttpStatus,
  Post,
  Get,
  Put,
  Param,
  ParseUUIDPipe,
  Delete, UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUsersDto } from './dto/create-users.dto';
import { UpdateUsersDto } from './dto/update-users.dto';
import { UpdateUserRolesDto } from './dto/update-user-roles.dto';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';
import {ApiTags} from "@nestjs/swagger";
import {JwtAuthGuard} from "../auth/guard/jwt-auth.guard";

@ApiTags('Users')
@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: CreateUsersDto) {
   try {
    await this.usersService.create(createUserDto)
    return {
      status: HttpStatus.CREATED,
      message: 'Success create SIPD Account',
    };
   } catch (error) {
   throw error
   }
  }

  @Get()
  async findAll() {
    const data = await this.usersService.findAll();

    return {
      data,
      status: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('/:id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
   try {
    return {
      data: await this.usersService.findOne(id),
      status: HttpStatus.OK,
      message: 'success',
    };
   } catch (error) {
   throw error
   }
  }

  @Put('/:id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserDto: UpdateUsersDto,
  ) {
    try {
      return {
        data: await this.usersService.updateUsers(id, updateUserDto),
        status: HttpStatus.OK,
        message: 'User data updated',
      };
    } catch (error) {
       throw error
    }
  }

  @Delete('/:id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    
    try {
      await this.usersService.remove(id);

      return {
        status: HttpStatus.OK,
        message: 'User deleted',
      };
    } catch (error) {
     throw error
    }
  }

  @Post('update-user-roles/:id')
  async updateRoles(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserRolesDto: UpdateUserRolesDto
  ){
    try {
      await this.usersService.updateUserRoles(id, updateUserRolesDto)
      return {
        status: HttpStatus.OK,
        message: 'User role updated',
      };
    } catch (error) {
     throw error
    }
  }

  @Post('/update-password/:id')
  async updatePassword(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserPasswordDto: UpdateUserPasswordDto,
  ) {
    try {
      await this.usersService.updatePassword(id, updateUserPasswordDto)
      return {
        status: HttpStatus.OK,
        message: 'User password updated',
      };
    } catch (error) {
     throw error
    }
  }

}
