import { add, endOfDay, startOfDay, sub, format } from 'date-fns';
import { parseFromTimeZone, formatToTimeZone } from 'date-fns-timezone';

export const startDay = startOfDay(new Date()).toISOString();
export const endDay = endOfDay(new Date()).toISOString();

export const parseHour = (timeType, value) => {
  switch (timeType) {
    case 'W':
      return value * 7 * 24 * 60;
    break;
  case 'D':
    return value * 24 * 60;
      break;
  case 'H':
      return value * 60;
    break;
  case 'M':
    return value;
    break;
  }
};

export const getCurrentDateJakarta = ({ dateOnly = false } = {}) => {
  const format = dateOnly ? 'YYYY-MM-DD' : 'YYYY-MM-DD HH:mm:ss';
  const currentTime = parseFromTimeZone(new Date().toISOString(), {
    timeZone: process.env.TIMEZONE || 'Asia/Jakarta',
  });

  return formatToTimeZone(currentTime, format, { timeZone: 'Asia/Jakarta' });
};

export const parseTimeZone = (date) => {
  const format = 'YYYY-MM-DD HH:mm:ss';
  const currentTime = parseFromTimeZone(date.toISOString(), {
    timeZone: process.env.TIMEZONE || 'Asia/Jakarta',
  });

  return formatToTimeZone(currentTime, format, { timeZone: 'Asia/Jakarta' });
};

export const getCurrentDateJakartaISO = ({ dateOnly = false } = {}) => {
  const format = 'YYYY-MM-DD[T]HH:mm:ss';
  const currentTime = parseFromTimeZone(new Date().toISOString(), {
    timeZone: process.env.TIMEZONE || 'Asia/Jakarta',
  });

  return formatToTimeZone(currentTime, format, { timeZone: 'Asia/Jakarta' });
};

export const parseJakartaTime = (time) => {
  const currentTime = getCurrentDateJakarta();
  const formatNew = format(new Date(currentTime), 'yyyy-MM-dd ') + time;

  return formatNew;
};

export const getStartDayJakarta = () => {
  const currentTime = new Date();
  let dateVal = new Date(currentTime);

  if (process.env.TIMEZONE === 'Etc/UTC') {
    dateVal = add(new Date(currentTime), { hours: 7 });
  }

  if (process.env.TIMEZONE === 'Etc/UTC') {
    return sub(startOfDay(dateVal), { hours: 7 }).toISOString();
  }

  return startOfDay(dateVal).toISOString();
};

export const getEndDayJakarta = () => {
  const currentTime = new Date();
  let dateVal = new Date(currentTime);

  if (process.env.TIMEZONE === 'Etc/UTC') {
    dateVal = add(new Date(currentTime), { hours: 7 });
  }

  if (process.env.TIMEZONE === 'Etc/UTC') {
    return sub(endOfDay(dateVal), { hours: 7 }).toISOString();
  }

  return endOfDay(dateVal).toISOString();
};

export const getStartDayJakartaWithParam = (date = null) => {
  const dateVal = date || new Date().toISOString();
  const currentTime = parseFromTimeZone(dateVal, {
    timeZone: process.env.TIMEZONE || 'Asia/Jakarta',
  });
  const startDate = startOfDay(
    new Date(
      formatToTimeZone(currentTime, 'YYYY-MM-DD', { timeZone: 'Asia/Jakarta' }),
    ),
  );

  if (process.env.TIMEZONE === 'Etc/UTC') {
    return sub(startDate, { hours: 7 }).toISOString();
  }

  return startDate.toISOString();
};

export const getEndDayJakartaWithParam = (date = null) => {
  const dateVal = date || new Date().toISOString();
  const currentTime = parseFromTimeZone(dateVal, {
    timeZone: process.env.TIMEZONE || 'Asia/Jakarta',
  });

  const endDate = endOfDay(
    new Date(
      formatToTimeZone(currentTime, 'YYYY-MM-DD', { timeZone: 'Asia/Jakarta' }),
    ),
  );

  if (process.env.TIMEZONE === 'Etc/UTC') {
    return sub(endDate, { hours: 7 }).toISOString();
  }

  return endDate.toISOString();
};

export const isHourPass = (hourStringOne, hourStringTwo) => {
  const intOne = parseInt(hourStringOne.replace(/:/g, ''));
  const intTwo = parseInt(hourStringTwo.replace(/:/g, ''));

  return intOne > intTwo;
};
